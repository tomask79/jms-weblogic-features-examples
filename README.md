Three examples using the weblogic's JMS features:

Unit of Order:

http://docs.oracle.com/cd/E13222_01/wls/docs103/jms/uoo.html

https://www.youtube.com/watch?v=B9J7q5NbXag

Unit of Work:

http://docs.oracle.com/cd/E13222_01/wls/docs92/jms/uow.html

https://www.youtube.com/watch?v=lZK016sALFo

I also added an example how you can use "Unit of Work" at weblogic using just Spring Integration.