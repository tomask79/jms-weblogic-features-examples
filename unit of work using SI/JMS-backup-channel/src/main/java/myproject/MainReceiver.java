package myproject;


import java.util.Random;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class MainReceiver {
	public static void main(String args[]) {
		FileSystemXmlApplicationContext appContext = 
				new FileSystemXmlApplicationContext("C:\\javatests\\JMS-backup-channel\\src\\main\\webapp\\WEB-INF\\applicationContext.xml");
		
		dispatchSend  fireService = (dispatchSend) appContext.getBean("fire");

		int i = 0;
		fireService.send("!! Watch this !!"+(++i), "my_unit", 1, false);
		fireService.send("!! Watch this !!"+(++i), "my_unit", 2, false);
		fireService.send("!! Watch this !!"+(++i), "my_unit", 3, false);
		fireService.send("!! Watch this !!"+(++i), "my_unit", 4, false);
		fireService.send("!! Watch this !!"+(++i), "my_unit", 5, true);
	}
}
