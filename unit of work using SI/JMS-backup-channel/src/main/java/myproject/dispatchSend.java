package myproject;

import org.springframework.integration.annotation.Gateway;
import org.springframework.messaging.handler.annotation.Header;

public interface dispatchSend {
	
	@Gateway(requestChannel="toJMS")
	public void send(String input, 
			@Header("JMS_BEA_UnitOfWork") String uow, 
			@Header("JMS_BEA_UnitOfWorkSequenceNumber") Integer sequence,
			@Header("JMS_BEA_IsUnitOfWorkEnd") Boolean endOfUow );
}
