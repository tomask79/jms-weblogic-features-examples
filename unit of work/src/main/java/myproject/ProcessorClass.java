package myproject;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.springframework.messaging.Message;

public class ProcessorClass {
	private final String mdbId = java.util.UUID.randomUUID().toString();
	
	public void processClass(final Message<?> input) {
		List<?> messages = (List<?>) input.getPayload();
		for (int i = 0;i < messages.size();i++) {
			TextMessage message = (TextMessage) messages.get(i);
			try {
				System.out.println("Unit of work part ["+i+"]"+message.getText());
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
