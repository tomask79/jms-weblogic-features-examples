package myproject;

import org.springframework.messaging.Message;

public class ProcessorClass {
	private final String mdbId = java.util.UUID.randomUUID().toString();
	
	public void processClass(final Message<?> input) {
		//synchronized(this) {
			String unitOfOrder = (String) input.getHeaders().get("JMS_BEA_UnitOfOrder");
			// Sleep for 2 seconds to demonstrate the messages are indeed
			// processes sequentially (in unit-of-order)
			System.out.println("UOOListener:: MDB=[" + mdbId + "] UOO=[" + unitOfOrder + "] Message=[" + input.getPayload() + "]");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//}
	}
}
