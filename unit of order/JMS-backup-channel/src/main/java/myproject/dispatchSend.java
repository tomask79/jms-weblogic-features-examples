package myproject;

import org.springframework.integration.annotation.Gateway;
import org.springframework.messaging.handler.annotation.Header;

public interface dispatchSend {
	
	@Gateway(requestChannel="toJMS")
	public void send(String input, @Header("JMSXGroupID") Integer groupId, @Header("JMSXGroupSeq") Integer sequence);
}
