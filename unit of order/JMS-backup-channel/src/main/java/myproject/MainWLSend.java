package myproject;

import java.io.*;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.QueueConnectionFactory;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import weblogic.jms.extensions.*;
	 
public class MainWLSend {
	public final static String JNDI_FACTORY="weblogic.jndi.WLInitialContextFactory";
	public final static String JMS_FACTORY="NativeConnectionFactory";
	public final static String QUEUE="NativeJMSQueue";
	 
	private QueueConnectionFactory qconFactory;
	private WLConnection qcon;
	private WLQueueSession qsession;
	private WLMessageProducer messageProducer;
	private WLDestination queue;
	private TextMessage msg;
	 
	public void init(Context ctx, String queueName) throws NamingException, JMSException
	{
		qconFactory = (QueueConnectionFactory) ctx.lookup(JMS_FACTORY);
		qcon = (WLConnection) qconFactory.createQueueConnection();
		qsession = (WLQueueSession) qcon.createQueueSession(false, 0);
		queue = (WLDestination) ctx.lookup(queueName);
		messageProducer = (WLMessageProducer) qsession.createProducer(queue);
		messageProducer.setUnitOfOrder("TEST2");
		msg = qsession.createTextMessage();
		qcon.start();
	}
	 
	public void send(String message) throws JMSException {
		msg.setText(message);
		messageProducer.send(msg);
	}
	 
	public void close() throws JMSException {
		messageProducer.close();
		qsession.close();
		qcon.close();
	}
	 
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.out.println("Usage: java QueueSend WebLogicURL");
			return;
		}
		
		InitialContext ic = getInitialContext(args[0]);
		MainWLSend qs = new MainWLSend();
		qs.init(ic, QUEUE);
		readAndSend(qs);
		qs.close();
	}
	 
	private static void readAndSend(MainWLSend qs) throws IOException, JMSException
	{
		for (int x = 1; x <= 20; x++)
		{
			String text = "Hello text nr. (" + x + ")";
			qs.send(text);
		}
	}
	 
	private static InitialContext getInitialContext(String url) throws NamingException
	{
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
		env.put(Context.PROVIDER_URL, url);
		return new InitialContext(env);
	}
}
